const bookData = [
  {
    id: '7',
    title: 'The Hunger Games',
    author: 'Suzanne Collins',
    price: 'Nu:350',
  },
  {
    id: '8',
    title: 'Harry Potter and the Sorcerer\'s Stone',
    author: 'J.K. Rowling',
    price: 'Nu:450',
  },
  {
    id: '9',
    title: 'The Lord of the Rings',
    author: 'J.R.R. Tolkien',
    price: 'Nu:550',
  },
  {
    id: '10',
    title: 'The Hitchhiker\'s Guide to the Galaxy',
    author: 'Douglas Adams',
    price: 'Nu:320',
  },
  {
    id: '11',
    title: 'Dune',
    author: 'Frank Herbert',
    price: 'Nu:480',
  },
  {
    id: '12',
    title: 'Great Work of Time',
    author: 'John Crowley',
    price: 'Nu:400',
  }

  
  ];
  export default bookData;